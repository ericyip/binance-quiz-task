import { useEffect, useRef, useState } from 'react';

export function useTimer(
  timeInSecond: number,
  callback?: VoidFunction,
) {
  const savedCB = useRef<VoidFunction>();
  const [timeLeft, setTimeLeft] = useState(timeInSecond);
  let id = useRef<NodeJS.Timeout>();

  useEffect(() => {
    savedCB.current = callback;
  }, [callback]);

  useEffect(() => {
    id.current = setInterval(() => {
      setTimeLeft((s) => s - 1);
    }, 1000);

    return () => {
      clearInterval(id.current);
    };
  }, []);

  useEffect(() => {
    if (timeLeft <= 0) {
      savedCB.current?.();
      clearInterval(id.current);
    }
  }, [timeLeft]);

  return timeLeft;
}
