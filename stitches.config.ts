import { createStyled } from '@stitches/react';

export const { styled, css } = createStyled({
  tokens: {
    colors: {
      $primary: 'rgb(240, 185, 11)', // binance color
    },
    space: {
      $1: '4px',
      $2: '8px',
      $3: '16px',
    },
    fontSizes: {
      $1: '12px',
      $2: '14px',
      $3: '16px',
    },
  },
});
