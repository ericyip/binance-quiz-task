import { styled } from '../stitches.config';

export const Button = styled('button', {
  appearance: 'none',
  borderRadius: '0.375rem',
  fontWeight: 600,
  height: '2.5rem',
  minWidth: '2.5rem',
  fontSize: '1rem',
  paddingLeft: '1rem',
  paddingRight: '1rem',
  backgroundColor: '$primary',
  color: 'rgb(255, 255, 255)',
  ':disabled': {
    backgroundColor: 'gray',
  },
});
