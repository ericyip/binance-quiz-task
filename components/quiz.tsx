import shuffle from 'lodash.shuffle';
import { useMemo, useState } from 'react';
import type { StitchesProps } from '@stitches/react';
import { styled } from '../stitches.config';
import type { Glossary } from '../types';
import { Button } from './button';
import { config } from '../config';
import { useTimer } from '../hooks/use-timer';

export function Quiz(props: {
  questions: Glossary[];
  allOptions: string[];
  onRestart?: () => void;
}) {
  const [step, setStep] = useState(0);
  const [finished, setFinished] = useState<
    {
      title: string;
      correct: boolean;
    }[]
  >([]);

  const compositeOptions: string[] = useMemo(() => {
    if (step >= props.questions.length) {
      return [];
    }
    return shuffle([
      props.questions[step].title,
      ...shuffle(
        props.allOptions.filter(
          (o) => o !== props.questions[step].title,
        ),
      ).slice(0, 3),
    ]);
  }, [step, props.questions]);

  if (step >= props.questions.length) {
    return (
      <div>
        <div>
          Scores: {finished.filter((f) => f.correct).length} /{' '}
          {props.questions.length}
        </div>
        <Button
          css={{
            marginTop: '$3',
          }}
          onClick={props.onRestart}
        >
          Restart
        </Button>
      </div>
    );
  }

  return (
    <div>
      <Question
        step={step}
        key={step}
        question={props.questions[step]}
        options={compositeOptions}
        onNext={(result) => {
          setStep((s) => s + 1);
          setFinished((s) => [...s, result]);
        }}
      />
    </div>
  );
}

const QuestionTitleWrapper = styled('div', {
  display: 'flex',
  justifyContent: 'space-between',
});

const QuestionNumber = styled('h3', {
  marginBottom: '$2',
});

const QuestionTitle = styled('h2', {
  marginBottom: '$3',
});

const ChoiceWrapper = styled('li', {
  marginBottom: '$2',
});

const Choice = styled('button', {
  color: 'black',
  padding: '$2',
  margin: '$1',
  borderRadius: 4,
  variants: {
    result: {
      correct: {
        backgroundColor: 'green',
        color: 'white',
      },
      incorrect: {
        backgroundColor: 'red',
        color: 'white',
      },
    },
  },
});

function Question(props: {
  question: Glossary;
  options: string[];
  step: number;
  onNext: (result: { correct: boolean; title: string }) => void;
}) {
  const [selected, setSelected] = useState<string>();
  const timeLeft = useTimer(config.timeConstraintInSecond);
  const timeIsUp = timeLeft <= 0;

  return (
    <div>
      <QuestionTitleWrapper>
        <QuestionNumber>Q {props.step + 1}</QuestionNumber>
        {!selected && <div>{timeLeft || "Time's up"}</div>}
      </QuestionTitleWrapper>
      <QuestionTitle>{props.question.excerpt}</QuestionTitle>
      <ol>
        {props.options.map((option) => {
          let result: StitchesProps<typeof Choice>['result'];
          if (selected || timeIsUp) {
            if (option === props.question.title) {
              result = 'correct';
            } else if (selected === option) {
              result = 'incorrect';
            }
          }

          return (
            <ChoiceWrapper key={option}>
              <Choice
                aria-pressed={selected === option}
                disabled={!!selected || timeIsUp}
                onClick={() => setSelected(option)}
                result={result}
              >
                {option}
              </Choice>
            </ChoiceWrapper>
          );
        })}
      </ol>
      <Button
        css={{
          marginTop: '$3',
        }}
        disabled={!selected && !timeIsUp}
        onClick={() => {
          props.onNext({
            title: props.question.title,
            correct: selected === props.question.title,
          });
        }}
      >
        Next
      </Button>
    </div>
  );
}
