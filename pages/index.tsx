import shuffle from 'lodash.shuffle';
import { GetStaticProps } from 'next';
import Head from 'next/head';
import { useMemo, useState } from 'react';
import { Button } from '../components/button';
import { Quiz } from '../components/quiz';
import { config } from '../config';
import dump from '../dump.json';
import { styled } from '../stitches.config';
import type { Glossary } from '../types';

const AppWrapper = styled('div', {
  maxWidth: '45rem',
  margin: 'auto',
  paddingLeft: '$1',
  paddingRight: '$1',
  marginTop: '50px',
});

export default function Home({ data }: { data: Glossary[] }) {
  const [isStarted, setIsStarted] = useState(false);

  const tenQuestions: Glossary[] = useMemo(() => {
    return shuffle(data).slice(0, config.total);
  }, [isStarted]);

  const allOptions = useMemo(() => {
    return data.map((d) => d.title);
  }, []);

  return (
    <>
      <Head>
        <title>Binance Glossaries Quiz</title>
      </Head>
      <AppWrapper>
        {isStarted ? (
          <Quiz
            allOptions={allOptions}
            questions={tenQuestions}
            onRestart={() => setIsStarted(false)}
          />
        ) : (
          <>
            <h1>Binance Glossaries Quiz!!!</h1>
            <Button
              css={{
                marginTop: '$3',
              }}
              onClick={() => setIsStarted(true)}
            >
              Let's go
            </Button>
          </>
        )}
      </AppWrapper>
    </>
  );
}

export const getStaticProps: GetStaticProps = async () => {
  let data: Glossary[];
  try {
    data = await getGlossaries();
  } catch (error) {
    data = dump as Glossary[]; // whatever the API is dead, we still get this hardcoded data
  }

  return {
    props: {
      data,
    },
    // let's assume we agree (at least me) the glossaries is not update frequently more than 1 day,
    // or else we can rebuild it by redeploying
    revalidate: 60 * 60 * 24, // cache it for one day.
  };
};

const getGlossaries = async (
  levelSlug?: Glossary['difficulty']['slug'],
) => {
  try {
    const result = await fetch(
      `https://api.binance.vision/api/glossaries/${levelSlug || ''}`,
    );
    const data = await result.json();
    return data as Glossary[];
  } catch (error) {
    console.log('log error here');
  }
};
