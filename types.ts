type BeginnerDifficulty = {
  label: 'Beginner';
  level: 1;
  slug: 'beginner';
};

type IntermediateDifficulty = {
  label: 'Intermediate';
  level: 2;
  slug: 'intermediate';
};

type AdvancedDifficulty = {
  label: 'Advanced';
  level: 3;
  slug: 'advanced';
};

export type Glossary = {
  difficulty:
    | BeginnerDifficulty
    | IntermediateDifficulty
    | AdvancedDifficulty;
  excerpt: string;
  slug: string;
  title: string;
};
