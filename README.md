### Requirement
- [x] Use the following endpoint in order to gather the list of glossary entries from Binance Academy. https://api.binance.vision/api/glossaries/
- [x] Build a quiz of 10 questions with the following requirements:
  - [x] Pick 10 glossary definitions at random
  - [x] Each question on the quiz will contain one definition which will be used for the question and 4 glossary titles (terms) which will be used for multiple choice answers.
  - [x] The user will need to match the definition with the correct glossary title (term)
  - [x] At the end of the quiz, display the user’s score.


Optional:
- [x] Add constraints on the user by incorporating a timer.
- [x] Make it responsive
- [ ] Feel free to add any additional constraints that you feel add value to the quiz.


## Explanation
1. Simple approach using Nextjs static generation, cache for static for 1 day (or redeployment), My assumption is the API data is not require to frequently update, (of course we could take that out and fetch data by useEffect)

2. Because this task is simple, I only use React to managing state.

3. used stitches to styled, because all style I suppose this task and even the academy don't need prop interpolations at runtime. Or you can use more advance compile style as static to free more runtime cost.

### out of scope
testing, normally I will do TDD (if I have time), or add some basic test.